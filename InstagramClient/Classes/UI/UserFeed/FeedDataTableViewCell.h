//
//  FeedDataTableViewCell.h
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LazyLoadingImageView;
@interface FeedDataTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet LazyLoadingImageView *mediaImageView;

@end
