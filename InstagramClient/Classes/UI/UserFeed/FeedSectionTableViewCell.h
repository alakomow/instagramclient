//
//  FeedSectionTableViewCell.h
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LazyLoadingImageView;

@interface FeedSectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet LazyLoadingImageView *userPicktureView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userFullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateLabel;
@end
