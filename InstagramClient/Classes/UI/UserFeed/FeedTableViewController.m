//
//  FeedTableViewController.m
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "FeedTableViewController.h"
#import "LazyLoadingImageView.h"

#import "UIAlertView+ErrorMessage.h"

#import "NSUserDefaults+AccessToken.h"

#import "NSManagedObjectContext+DataBaseHelper.h"
#import "NSManagedObject+Wrapper.h"
#import "User.h"
#import "Media.h"

#import "FeedSectionTableViewCell.h"
#import "FeedDataTableViewCell.h"

#import "APIOperation.h"
#import "MappingOperation.h"

#import "PhotoDetailsViewController.h"

#define kHeightForHeaderSection 88.f
#define kCellIdentifier @"FeedCellData"

typedef NS_ENUM(NSUInteger, LoadingState) {
    LoadingStateNone = 0,
    LoadingStateUpdateFollows,
    LoadingStateUpdateFeed
};

@interface FeedTableViewController ()<NSFetchedResultsControllerDelegate, UIScrollViewDelegate> {
    LazyLoadingImageView *_profilePicktureView;
    NSFetchedResultsController *_contentController;
    BOOL _sectionsChanged;
    LoadingState _downloadingState;
    NSOperationQueue *_downLoadRecentOperationQueue;

    NSDateFormatter *_dateFromater;
}

@property(nonatomic, strong) UIRefreshControl *topRefreshControl;
@property(nonatomic, strong) UIView *bottomRefreshControl;

@end

@implementation FeedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _downLoadRecentOperationQueue = [[NSOperationQueue alloc] init];
    _downLoadRecentOperationQueue.maxConcurrentOperationCount = 3;

    [self.navigationItem setTitle:[self.user.fullName stringByAppendingString:@" (Feed)"]];
    CGFloat pictureViewSize = CGRectGetHeight(self.navigationController.navigationBar.frame) - 5.f;
    _profilePicktureView = [[LazyLoadingImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, pictureViewSize, pictureViewSize)];
    _profilePicktureView.layer.cornerRadius = pictureViewSize/2.f;
    _profilePicktureView.clipsToBounds = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_profilePicktureView];

    [_profilePicktureView loadImageFromURL:self.user.profilePicture];

    _dateFromater = [[NSDateFormatter alloc] init];
    [_dateFromater setLocale:[NSLocale currentLocale]];
    [_dateFromater setDateStyle:NSDateFormatterFullStyle];
    [_dateFromater setTimeStyle:NSDateFormatterShortStyle];

    _topRefreshControl = [[UIRefreshControl alloc] init];
    _topRefreshControl.tintColor = [UIColor grayColor];
    [_topRefreshControl addTarget:self action:@selector(updateFollows) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_topRefreshControl];
    self.tableView.alwaysBounceVertical = YES;


    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(indicator.frame) * 2.f)];
        [bottomView addSubview:indicator];
        indicator.center = CGPointMake(CGRectGetWidth(bottomView.frame)/2.f, CGRectGetHeight(bottomView.frame)/2.f);
        [indicator startAnimating];
        _bottomRefreshControl = bottomView;

    [self updateFollows];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    _contentController  = nil;
    [self.tableView reloadData];
}

#pragma mark -
- (NSFetchedResultsController *)contentController {
    if (_contentController)
        return _contentController;

    NSFetchRequest *request = [Media entityFetchRequestInContext:[NSManagedObjectContext defaultContext]];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.folowedBy.userID CONTAINS[cd] %@",self.user.userID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdTime" ascending:NO];

    request.predicate = predicate;
    request.sortDescriptors = @[sortDescriptor];

    NSString *cacheName = NSStringFromClass([self class]);
    [NSFetchedResultsController deleteCacheWithName:cacheName];

    _contentController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext defaultContext] sectionNameKeyPath:@"mediaID" cacheName:cacheName];
    _contentController.delegate = self;

    NSError *error;
    if (![_contentController performFetch:&error]) {
        [[[UIAlertView alloc] initWithError:error completion:NULL] show];
    }
    return _contentController;
}

- (void)updateFollows {

    if (!self.user || _downloadingState){
        [self.topRefreshControl endRefreshing];
        return;
    }

    if (!self.topRefreshControl.isRefreshing) {
        [self.topRefreshControl beginRefreshing];

        if (self.tableView.contentOffset.y == 0) {
            [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){

                self.tableView.contentOffset = CGPointMake(0, -self.topRefreshControl.frame.size.height);

            } completion:^(BOOL finished){

            }];

        }
    }



    __weak typeof(self) weakSelf = self;

    _downloadingState = LoadingStateUpdateFollows;
    [[[APIOperation alloc] initFollowsForUserID:self.user.userID accessToken:[NSUserDefaults accessToken] completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
       if (error){
            if(didCanceled) {
                [weakSelf updatingFollowsDidFinishedWithObjects:nil error:nil inContext:nil];
                return;
            }
           [weakSelf updatingFollowsDidFinishedWithObjects:nil error:error inContext:nil];
           return ;
        }

        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[User class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            if (error) {
                [weakSelf updatingFollowsDidFinishedWithObjects:nil error:error inContext:nil];
                return;
            }

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                User *curUser = (User *)[context objectWithID:weakSelf.user.objectID];
                if (curUser) {
                    [curUser setFollows:[NSSet new]];
                    for (User *object in objects) {
                        [curUser addFollowsObject:object];
                    }
                    [context saveWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        [weakSelf updatingFollowsDidFinishedWithObjects:objects error:error inContext:context];
                    }];
                }
            });
        }] start];
    }] start];
}

- (void)updatingFollowsDidFinishedWithObjects:(NSArray *)objects error:(NSError *)error inContext:(NSManagedObjectContext *)context {
    if (error) {
        [[[UIAlertView alloc] initWithError:error completion:^{
            [self.topRefreshControl endRefreshing];
            _downloadingState = LoadingStateNone;
        }] show];
        return;
    }
    [self updateUserFeed];
}

- (void)updateUserFeed {
    NSArray *users = [self.user.follows allObjects];
    _downloadingState = LoadingStateUpdateFeed;
    __weak typeof(self) weakSelf = self;
    __weak NSOperationQueue *weakOperationQueue = _downLoadRecentOperationQueue;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        for (User *user in users) {
            [_downLoadRecentOperationQueue addOperationWithBlock:^{
                [weakSelf loadRecentMediaForUser:user maxMediadate:nil];
                if (weakOperationQueue.operationCount == 1) {
                     _downloadingState = LoadingStateNone;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf.topRefreshControl endRefreshing];
                    });
                }
            }];
        }
        if (!users.count) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.topRefreshControl endRefreshing];
            });
        }

    });
}
- (void)loadRecentMediaForUser:(User *)user maxMediadate:(NSDate *)date {
    if (!user.userID) return;
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    APIOperation *loadOperation = [[APIOperation alloc] initMediaRecentForIsedID:user.userID accessToken:[NSUserDefaults accessToken] maxDate:date completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (error) {
            //TODO: Implement handling error.
            dispatch_semaphore_signal(semaphore);
            return;
        }

        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[Media class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            if (error) {
                //TODO: Implement handling error.
                dispatch_semaphore_signal(semaphore);
                return;
            }
            dispatch_semaphore_signal(semaphore);
        }] start];
    }];
    [loadOperation start];
    dispatch_time_t timeoutTime = dispatch_time(DISPATCH_TIME_NOW,(int64_t)(60 * NSEC_PER_SEC));
    if (dispatch_semaphore_wait(semaphore, timeoutTime)) {
        [loadOperation cancel];
        //TODO: Implemet rase error timeOut
    }
}

- (NSDate *)lastMediaDateForUser:(User *)user {
    NSFetchRequest *request = [Media entityFetchRequestInContext:[NSManagedObjectContext defaultContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.userID == %@",self.user.userID];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdTime" ascending:YES];
    request.fetchLimit = 1;

    request.predicate = predicate;
    request.sortDescriptors = @[sortDescriptor];

    NSError *error;
    Media *mediaObject = [[NSManagedObjectContext defaultContext] executeFetchRequest:request error:&error].firstObject;
    return mediaObject.createdTime;
}

- (void)loadMoreRecentMedia {

    if (!self.user || _downloadingState) {
        return;
    }
    _downloadingState = LoadingStateUpdateFeed;

    typeof(self) weakSelf = self;
    NSSet *users = self.user.follows;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (User *user in users) {
            NSDate *lastMediaCreatedDate = [weakSelf lastMediaDateForUser:user];
            [_downLoadRecentOperationQueue addOperationWithBlock:^{
                [weakSelf loadRecentMediaForUser:user maxMediadate:lastMediaCreatedDate];
                if (_downLoadRecentOperationQueue.operationCount == 1) {
                    [weakSelf loadMoreDidFinished];
                }
            }];
        }
    });
}

- (void)loadMoreDidFinished {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIEdgeInsets tableContentInsets = self.tableView.contentInset;
        tableContentInsets.bottom = 0.f;
        [UIView animateWithDuration:0.5f animations:^{
            self.tableView.contentInset = tableContentInsets;
        } completion:^(BOOL finished) {
            self.tableView.tableFooterView = nil;
            _downloadingState = LoadingStateNone;
        }];
    });
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        default:
            break;
//        case NSFetchedResultsChangeUpdate:
//            [tableView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            _sectionsChanged = YES;
//            break;
    }

}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeMove:
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - Table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self contentController].sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[[self contentController] sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self contentController] sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *identifier = @"FeedCellSection";
    FeedSectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    Media *object = [[self contentController] objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    cell.userNameLabel.text = object.user.userName;
    cell.userFullNameLabel.text = object.user.fullName;
    cell.postDateLabel.text = [_dateFromater stringFromDate:object.createdTime];
    [cell.userPicktureView loadImageFromURL:object.user.profilePicture];
    cell.userPicktureView.layer.cornerRadius = CGRectGetHeight(cell.userPicktureView.frame)/2.f;
    [cell.userPicktureView setClipsToBounds:YES];
    return [cell contentView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = kCellIdentifier;
    FeedDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    Media *media = [[self contentController] objectAtIndexPath:indexPath];
    [cell.mediaImageView loadImageFromURL:media.imageURL];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kHeightForHeaderSection;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.contentSize.width;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id media = [[self contentController] objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ShowDetailPhoto" sender:media];
}

#pragma mark - Observers
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqual:@"follows"]) {
            _contentController = nil;
        [self contentController];
    } else
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.y + CGRectGetHeight(scrollView.frame) ) >= scrollView.contentSize.height && scrollView.contentSize.height)
    {
        if (![self contentController].fetchedObjects.count) return;
        if (_downloadingState) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIEdgeInsets contentInsets = self.tableView.contentInset;
            contentInsets.bottom = CGRectGetHeight(self.bottomRefreshControl.frame) + 40.f;
            self.tableView.tableFooterView = self.bottomRefreshControl;
            [UIView animateWithDuration:0.5 animations:^{
                self.tableView.contentInset = contentInsets;
            } completion:^(BOOL finished) {
                [self loadMoreRecentMedia];
            }];
        });
    }
}


- (void)dealloc {
    [_downLoadRecentOperationQueue cancelAllOperations];
    _downLoadRecentOperationQueue = nil;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowDetailPhoto"] && [sender isKindOfClass:[Media class]]) {
        PhotoDetailsViewController *controller = segue.destinationViewController;
        controller.mediaObject = sender;
    }
}


@end
