//
//  CommentTableViewCell.h
//  InstagramClient
//
//  Created by Artem on 24/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LazyLoadingImageView.h"
@interface CommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet LazyLoadingImageView *userPictureView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *comminrDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;

@end
