//
//  PhotoDetailImageCell.h
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LazyLoadingImageView.h"

@interface PhotoDetailImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet LazyLoadingImageView *photoView;

@end
