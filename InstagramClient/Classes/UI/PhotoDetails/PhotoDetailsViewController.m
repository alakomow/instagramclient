//
//  PhotoDetailsViewController.m
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "PhotoDetailsViewController.h"
#import "PhotoDetailImageCell.h"
#import "CommentTableViewCell.h"

#import "UIAlertView+ErrorMessage.h"

#import "APIOperation.h"
#import "MappingOperation.h"

#import "NSUserDefaults+AccessToken.h"
#import "NSManagedObjectContext+DataBaseHelper.h"
#import "NSManagedObject+Wrapper.h"

#import "Media.h"
#import "Comment.h"
#import "User.h"

typedef NS_ENUM(NSUInteger, LoadingState) {
    LoadingStateNone = 0,
    LoadingStateLoadingComment
};

@interface PhotoDetailsViewController ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>{
    LoadingState _loadingState;
    APIOperation *_loadingOperation;

    NSDateFormatter *_dateFromater;
}
@property (strong, nonatomic) UIRefreshControl *topRefreshControl;
@property (strong, nonatomic) NSFetchedResultsController *contentController;
@end

@implementation PhotoDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dateFromater = [[NSDateFormatter alloc] init];
    [_dateFromater setLocale:[NSLocale currentLocale]];
    [_dateFromater setDateStyle:NSDateFormatterShortStyle];
    [_dateFromater setTimeStyle:NSDateFormatterShortStyle];

    _topRefreshControl = [[UIRefreshControl alloc] init];
    _topRefreshControl.tintColor = [UIColor grayColor];
    [_topRefreshControl addTarget:self action:@selector(updateComments) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_topRefreshControl];
    self.tableView.alwaysBounceVertical = YES;

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 2.f;

    [self updateComments];

    self.navigationItem.title = NSLocalizedString(@"PhotoComments", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [_loadingOperation cancel];
    _loadingOperation = nil;
}

#pragma mark -
- (void)updateComments {
    if (!self.mediaObject || _loadingState){
        [self.topRefreshControl endRefreshing];
        return;
    }

    _loadingState = LoadingStateLoadingComment;

    if (!self.topRefreshControl.isRefreshing) {
        [self.topRefreshControl beginRefreshing];

        if (self.tableView.contentOffset.y == 0) {
            [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){

                self.tableView.contentOffset = CGPointMake(0, -self.topRefreshControl.frame.size.height);

            } completion:^(BOOL finished){
                
            }];
            
        }
    }

    typeof(self) weakSelf = self;
    _loadingOperation = [[APIOperation alloc] initCommentWithMediaID:self.mediaObject.mediaID accessToken:[NSUserDefaults accessToken] completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            [weakSelf handleOperationFinish];
            return;
        }

        if (error) {
            [weakSelf hanleError:error];
            return;
        }

        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[Comment class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            if (error) {
                [weakSelf hanleError:error];
                return;
            }
            Media *curMediaObject = (Media *)[context objectWithID:weakSelf.mediaObject.objectID];
            [curMediaObject addComments:[NSSet setWithArray:objects]];
            [context saveWithCompletion:^(BOOL contextDidSave, NSError *error) {
                if (error) {
                    [weakSelf hanleError:error];
                    return;
                }
                [self handleOperationFinish];
            }];
        }] start];

    }];
    [_loadingOperation start];
}

#pragma mark - loading wrappers
- (void)hanleError:(NSError *)error {
    [[[UIAlertView alloc] initWithError:error completion:^{

    }] show];
    _loadingOperation = nil;
    _loadingState = LoadingStateNone;
    [self.topRefreshControl endRefreshing];
}

- (void)handleOperationFinish {
    _loadingOperation = nil;
    _loadingState = LoadingStateNone;
    [self.topRefreshControl endRefreshing];
}

#pragma mark - NSFetchedResultsController
- (NSFetchedResultsController *)contentController {
    if (_contentController)
        return _contentController;

    NSFetchRequest *request = [Comment entityFetchRequestInContext:[NSManagedObjectContext defaultContext]];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"media.mediaID == %@",self.mediaObject.mediaID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdTime" ascending:NO];

    request.predicate = predicate;
    request.sortDescriptors = @[sortDescriptor];

    NSString *cacheName = NSStringFromClass([self class]);
    [NSFetchedResultsController deleteCacheWithName:cacheName];

    _contentController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext defaultContext] sectionNameKeyPath:nil cacheName:cacheName];
    _contentController.delegate = self;

    NSError *error;
    if (![_contentController performFetch:&error]) {
        [[[UIAlertView alloc] initWithError:error completion:NULL] show];
    }
    return _contentController;
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[[self contentController] sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self contentController] sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CommentDataCell";
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(CommentTableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    Comment *comment = [self.contentController objectAtIndexPath:indexPath];
    cell.userNameLabel.text = comment.user.userName;
    cell.comminrDateLabel.text = [_dateFromater stringFromDate:comment.createdTime];
    cell.commentTextLabel.text = comment.text;
    [cell.userPictureView loadImageFromURL:comment.user.profilePicture];
    cell.userPictureView.layer.cornerRadius = CGRectGetHeight(cell.userPictureView.frame)/2.f;
    [cell.userPictureView setClipsToBounds:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PhotoDetailImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoCell"];
    [cell.photoView loadImageFromURL:self.mediaObject.imageURL];
    return [cell contentView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGRectGetWidth(tableView.frame);
}
@end
