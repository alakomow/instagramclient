//
//  LazyLoadingImageView.m
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "LazyLoadingImageView.h"

@interface LazyLoadingImageView()<NSURLConnectionDataDelegate, NSURLConnectionDelegate> {
    NSURLConnection *_connection;
    NSMutableData *_imageData;
    UIActivityIndicatorView *_loadingView;
}
@end

@implementation LazyLoadingImageView

+ (NSOperationQueue *)downloadQueue {
    static NSOperationQueue *operation = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        operation = [[NSOperationQueue alloc] init];
    });

    return operation;
}

- (void)loadImageFromURL:(NSString *)url {

    self.image = nil;

    [_connection cancel];
    _imageData = [NSMutableData new];
    [_loadingView removeFromSuperview];

    [self stopLoadingUI];
    [self startLoadingUI];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                             cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                         timeoutInterval:60.f];

    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [_connection setDelegateQueue:[[self class] downloadQueue]];
    [_connection start];
}

#pragma mark -
- (void)stopLoadingUI {
    [_loadingView stopAnimating];
    [_loadingView removeFromSuperview];
}

- (void)startLoadingUI {
    if (!_loadingView)
    _loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _loadingView.center = self.center;
    [self addSubview:_loadingView];
    [_loadingView startAnimating];
}

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_imageData appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    UIImage *image = [UIImage imageWithData:_imageData];

    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.image = image;
        [weakSelf stopLoadingUI];
    });
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    _imageData = nil;
    _connection = nil;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf stopLoadingUI];
    });
}
@end
