//
//  LazyLoadingImageView.h
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LazyLoadingImageView : UIImageView
- (void)loadImageFromURL:(NSString *)url;
@end
