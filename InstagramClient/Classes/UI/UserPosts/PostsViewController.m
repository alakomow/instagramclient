//
//  PostsViewController.m
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "PostsViewController.h"

#import "APIOperation.h"
#import "MappingOperation.h"

#import "UINavigationController+Authorization.h"
#import "UIAlertView+ErrorMessage.h"

#import "NSManagedObjectContext+DataBaseHelper.h"
#import "NSManagedObject+Wrapper.h"
#import "User.h"
#import "Media.h"

#import "NSUserDefaults+AccessToken.h"

#import "MediaCollectionViewCell.h"

#import "FeedTableViewController.h"
#import "PhotoDetailsViewController.h"
#import "UserSearchViewController.h"

#define kAdditionalHeigthForCell 48.f

typedef NS_ENUM(NSUInteger, LoadingState) {
    LoadingStateNone = 0,
    LoadingStateLoadingPosts
};

@interface PostsViewController ()<NSFetchedResultsControllerDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UserSearchViewControllerDelegate> {
    APIOperation *_downloadOperation;
    NSFetchedResultsController *_contentController;
    NSDateFormatter *_dateFromater;

    LoadingState _loadingState;

    BOOL isLoadingMore;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;
@property (strong, nonatomic) UIRefreshControl *topRefreshControll;

@property (nonatomic, strong) User *currentUser;
@end

@implementation PostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _dateFromater = [[NSDateFormatter alloc] init];
    [_dateFromater setLocale:[NSLocale currentLocale]];
    [_dateFromater setDateStyle:NSDateFormatterFullStyle];
    [_dateFromater setTimeStyle:NSDateFormatterShortStyle];

    _topRefreshControll = [[UIRefreshControl alloc] init];
    _topRefreshControll.tintColor = [UIColor grayColor];
    [_topRefreshControll addTarget:self action:@selector(refreshUserPosts) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:_topRefreshControll];
    self.collectionView.alwaysBounceVertical = YES;

    self.navigationItem.titleView = self.searchBar;
    self.currentUser = nil;

    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController checkAPIAuthorization];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_downloadOperation cancel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [_downloadOperation cancel];
}

#pragma mark - Setters
- (void)setCurrentUser:(User *)currentUser {
    BOOL isNeedReloadFetchController = !currentUser || ![_currentUser.userID isEqualToNumber:currentUser.userID];
    _currentUser = currentUser;

    if (isNeedReloadFetchController) {
        _contentController = nil;
        [self.collectionView reloadData];
    }

    [self.navigationItem.rightBarButtonItem setEnabled:_currentUser != nil];
}

#pragma mark -

- (NSFetchedResultsController *)contentController {
    if (_contentController)
        return _contentController;

    NSFetchRequest *request = [Media entityFetchRequestInContext:[NSManagedObjectContext defaultContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.userID == %@",_currentUser.userID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdTime" ascending:NO];

    request.predicate = predicate;
    request.sortDescriptors = @[sortDescriptor];

    NSString *cacheName = NSStringFromClass([self class]);
    [NSFetchedResultsController deleteCacheWithName:cacheName];

    _contentController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext defaultContext] sectionNameKeyPath:nil cacheName:cacheName];
    _contentController.delegate = self;

    NSError *error;
    if (![_contentController performFetch:&error]) {
        [[[UIAlertView alloc] initWithError:error completion:NULL] show];
    }
    return _contentController;
}

- (void)enableMoreLoadActivity:(BOOL)enabled {
    if (enabled) {
        UIEdgeInsets contentInset = self.collectionView.contentInset;
        self.collectionView.contentInset = UIEdgeInsetsMake(contentInset.top,
                                                            contentInset.left,
                                                            CGRectGetHeight(self.loadingActivity.frame) * 2.f,
                                                            contentInset.right);

        self.loadingActivity.center = CGPointMake(CGRectGetWidth(self.collectionView.frame)/2.f, CGRectGetHeight(self.collectionView.bounds) - CGRectGetHeight(self.loadingActivity.frame));
        [self.view addSubview:self.loadingActivity];
        [self.loadingActivity startAnimating];
    } else {
        [self.loadingActivity stopAnimating];
        [self.loadingActivity removeFromSuperview];
        UIEdgeInsets contentInset = self.collectionView.contentInset;
        self.collectionView.contentInset = UIEdgeInsetsMake(contentInset.top,
                                                            contentInset.left,
                                                            0.f,
                                                            contentInset.right);
    }
}

- (void)refreshUserPosts {

    if (_loadingState) {
        return;
    }

    _loadingState = LoadingStateLoadingPosts;

    __weak typeof(self) weakSelf = self;
    _downloadOperation = [[APIOperation alloc] initMediaRecentForIsedID:self.currentUser.userID accessToken:[NSUserDefaults accessToken] maxDate:nil completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            [weakSelf.topRefreshControll endRefreshing];
            [weakSelf handleLoadingFinish];
            return;
        }

        if (error) {
            [weakSelf handleLoadingError:error];
            [weakSelf.collectionView reloadData];
            [weakSelf.topRefreshControll endRefreshing];
            return;
        }

        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[Media class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            [weakSelf.topRefreshControll endRefreshing];
            if (error) {
                [weakSelf handleLoadingError:error];
                return;
            }
            [weakSelf handleLoadingFinish];
        }] start];

    }];
    [_downloadOperation start];
}

- (void)loadMorePosts {

    if (_loadingState) return;

    _loadingState = LoadingStateLoadingPosts;
    
    Media *media = [self contentController].fetchedObjects.lastObject;
    __weak typeof(self) weakSelf = self;
    _downloadOperation = [[APIOperation alloc] initMediaRecentForIsedID:self.currentUser.userID accessToken:[NSUserDefaults accessToken] maxDate:media.createdTime completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            [weakSelf enableMoreLoadActivity:NO];
            [weakSelf handleLoadingFinish];
            return;
        }

        if (error) {
            [weakSelf handleLoadingError:error];
            [weakSelf.collectionView reloadData];
            [weakSelf enableMoreLoadActivity:NO];
            return;
        }

        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[Media class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            [weakSelf enableMoreLoadActivity:NO];
            if (error) {
                [weakSelf handleLoadingError:error];
                return;
            }
            [weakSelf handleLoadingFinish];
        }] start];

    }];
    [_downloadOperation start];
}

- (void)tryLoadUserPosts {
    self.currentUser = [User findFirstByAtribute:@"userName" value:self.searchBar.placeholder inContext:[NSManagedObjectContext defaultContext]];

    if (self.currentUser) {
        [self.topRefreshControll beginRefreshing];

        if (self.collectionView.contentOffset.y == 0) {
            UIEdgeInsets contentInsets = self.collectionView.contentInset;
            contentInsets.top += self.topRefreshControll.frame.size.height;
            [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){
                self.collectionView.contentInset = contentInsets;
            } completion:^(BOOL finished) {
                [self refreshUserPosts];
            }];
            return;
        }
        [self refreshUserPosts];
    }
}

- (void)handleLoadingFinish {
    _loadingState = LoadingStateNone;
}
- (void)handleLoadingError:(NSError *)error {
    [[[UIAlertView alloc] initWithError:error completion:^{

    }] show];
    _loadingState = LoadingStateNone;
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [self performSegueWithIdentifier:@"SearchUserScreen" sender:nil];
    return NO;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self contentController].sections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([[[self contentController] sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self contentController] sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Media *media = [[self contentController] objectAtIndexPath:indexPath];
    MediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCell" forIndexPath:indexPath];
    cell.likeCountLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"LikesTitle", nil),media.likeCount];
    cell.dateLabel.text = [_dateFromater stringFromDate:media.createdTime];
    [cell.imageView loadImageFromURL:media.imageURL];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger countCellInRow = 2;
    CGFloat size = CGRectGetWidth(collectionView.frame)/countCellInRow  - countCellInRow;
    return CGSizeMake(size, size + kAdditionalHeigthForCell);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    id media = [[self contentController] objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"ShowDetailPhoto" sender:media];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.collectionView reloadData];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.contentOffset.y + CGRectGetHeight(scrollView.frame) ) >= scrollView.contentSize.height && scrollView.contentSize.height)
    {
        if (![self contentController].fetchedObjects.count) return;
        if (_loadingState) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self enableMoreLoadActivity:YES];
            [self loadMorePosts];
        });
    }
}

#pragma mark - UserSearchViewControllerDelegate
- (void)userSearchController:(UserSearchViewController *)controller didFoundUser:(User *)user {
    self.currentUser = user;
    self.searchBar.placeholder = user.userName;
    self.searchBar.text = nil;
    [self tryLoadUserPosts];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowFeed"]) {
        FeedTableViewController *controller = segue.destinationViewController;
        controller.user = self.currentUser;
    } else if ([segue.identifier isEqualToString:@"ShowDetailPhoto"] && [sender isKindOfClass:[Media class]]) {
        PhotoDetailsViewController *controller = segue.destinationViewController;
        controller.mediaObject = sender;
    } else if ([segue.identifier isEqualToString:@"SearchUserScreen"]) {
        UserSearchViewController *controller = segue.destinationViewController;
        controller.delegate = self;
    }
}


@end
