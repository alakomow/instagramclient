//
//  PostsViewController.h
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
