//
//  MediaCollectionViewCell.h
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LazyLoadingImageView.h"
@interface MediaCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet LazyLoadingImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
