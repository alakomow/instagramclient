//
//  LandingViewController.m
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//


#import <CoreLocation/CoreLocation.h>
#import <stdlib.h>

#import "LandingViewController.h"
#import "APIOperation.h"
#import "NSUserDefaults+AccessToken.h"

#import "UIAlertView+ErrorMessage.h"
#import "ParallaxView.h"

#define kTimerInterval 1.f

#define kMinSearchDistance 1000
#define kMaxSearchDistance 5000

#define kMinFaceSize 100
#define kMaxFaceSize 200

#define kMaxApiReloadRequest 5

#define kMaxUser 100

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)


typedef NS_ENUM(NSUInteger, LoadingState) {
    LoadingStateNone = 0,
    LoadingStateRunning,
    LoadingStateLoadingError
};

@interface LandingViewController () <CLLocationManagerDelegate> {
    NSUInteger _duration;
    CLLocationManager *_locationManager;
    NSTimer *_timer;
    APIOperation *_downloadOperation;
    LoadingState _loadingState;

    NSArray *_imageData;

    NSUInteger _requestCount;
}

@end

@implementation LandingViewController
- (void)viewDidLoad {
    [super viewDidLoad];

    _requestCount = 0;

    _locationManager = [CLLocationManager new];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    [_locationManager requestAlwaysAuthorization];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];

    [self reloadUsers];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self restartTimer];
    [self reloadAnimationForItems:_imageData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopTimer];

    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    for (UIView *view in _imageData) {
        [view removeFromSuperview];
    }
    _imageData = nil;
    [_downloadOperation cancel];
    _downloadOperation = nil;
}

#pragma mark - animations
- (void)reloadAnimationForItems:(NSArray *)items {

    for (UIView *view in self.view.subviews) {
        [view removeFromSuperview];
    }

    CGFloat maxYPosition = CGRectGetMaxY(self.view.frame);
    CGFloat maxXPosition = CGRectGetMaxX(self.view.frame);

    NSMutableArray *images = [NSMutableArray new];
    for (NSString *pic in items) {

        CGFloat faceSize = [self randomValueMinValue:kMinFaceSize maxValue:kMaxFaceSize];
        ParallaxView *imageView = [[ParallaxView alloc] initWithFrame:CGRectMake(0.f, 0.f, faceSize, faceSize)
                                                       centerPosition:CGPointMake([self randomValueMinValue:0
                                                                                                   maxValue:maxXPosition],
                                                                                  [self randomValueMinValue:0
                                                                                                   maxValue:maxYPosition])
                                                             imageURL:pic];

        CGPoint movePoing = CGPointMake([self randomValueMinValue:0
                                                         maxValue:maxXPosition],
                                        [self randomValueMinValue:0
                                                         maxValue:maxYPosition]);
        [imageView addMoveAnitationToPoint:movePoing];

        imageView.layer.cornerRadius = faceSize/2.f;
        imageView.clipsToBounds = YES;
        [images addObject:imageView];
        [self.view addSubview:imageView];
    }
}


#pragma mark -
- (int)randomValueMinValue:(int)min maxValue:(int)max {
  int val =  rand() % (max - min) + min;
    return val;
}

- (void)requestUsers {

    if (_loadingState) return;
    if (![NSUserDefaults accessToken].length) return;

    _loadingState = LoadingStateRunning;
    _requestCount++;
    if (_requestCount > kMaxApiReloadRequest) {
        _loadingState = LoadingStateLoadingError;
        return;
    }

    __weak typeof(self) weakSeft = self;
   _downloadOperation = [[APIOperation alloc] initSearchLocationWithAccessToken:[NSUserDefaults accessToken]
                                                                            lat:@(_locationManager.location.coordinate.latitude)
                                                                            lon:@(_locationManager.location.coordinate.longitude)
                                                                       distance:@([self randomValueMinValue:kMinSearchDistance
                                                                                                   maxValue:kMaxSearchDistance])
                                                                     completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
       if (error) {
           if (didCanceled) {
               return;
           }
           [weakSeft requestUserDidError:error];
           return;
       }
       NSSet *locationIds = [responce valueForKeyPath:@"data.id"];
        NSString *accessToken = [NSUserDefaults accessToken];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            for (id locationId in locationIds) {
                [[[APIOperation alloc] initSearchMediaRecentWithLocationID:locationId accessToken:accessToken completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
                    if (error && !didCanceled) {
                        [weakSeft requestUserDidError:error];
                    }
                    NSMutableSet *profilesPicUrl = [NSMutableSet new];

                    id mediaUsers = [responce valueForKeyPath:@"data.user.profile_picture"];
                    if ([mediaUsers isKindOfClass:[NSSet class]]) {
                        [profilesPicUrl unionSet:mediaUsers];
                    } else if ([mediaUsers isKindOfClass:[NSArray class]]) {
                        [profilesPicUrl addObjectsFromArray:mediaUsers];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSeft finishLoadUsers:profilesPicUrl.allObjects];
                    });
                    
                }] start];
            }
        });
   }];
    [_downloadOperation start];
}

- (void)requestUserDidError:(NSError *)error {
    [[[UIAlertView alloc] initWithError:error completion:^{

    }] show];
    _loadingState = LoadingStateLoadingError;
    [_downloadOperation cancel];
    _downloadOperation = nil;
}

- (void)finishLoadUsers:(NSArray *)usersPics {
    _downloadOperation = nil;
    if (!usersPics.count) return;
    if (_imageData.count > kMaxUser) return;

    _imageData = [usersPics arrayByAddingObjectsFromArray:_imageData];
    [self reloadAnimationForItems:_imageData];
}

- (void)reloadUsers {
    if (_loadingState == LoadingStateLoadingError) {
        [self stopTimer];
    }
    if (!_imageData.count) {
       [self requestUsers];
        return;
    }

    [_timer invalidate];
    _timer = nil;
}

#pragma mark - timer wrappers
- (void)stopTimer {
    if (_timer) {
        [_timer  invalidate];
        _timer = nil;
    }
}

- (void)startTimer {
    _timer = [NSTimer scheduledTimerWithTimeInterval:kTimerInterval target:self selector:@selector(reloadUsers) userInfo:nil repeats:YES];
}

- (void)restartTimer {
    [self stopTimer];
    [self startTimer];
}

#pragma mark  - Notification center
- (void)appWillResignActive {
    [self stopTimer];
}

- (void)appDidBecomeActive {
    [self restartTimer];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//TODO implement error handling
}

- (void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error {
//TODO implement error handling
}

#pragma mark - dealoc
- (void)dealloc {
    [self stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
