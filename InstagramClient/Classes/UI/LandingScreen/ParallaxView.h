//
//  ParallaxView.h
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParallaxView : UIView
- (instancetype)initWithFrame:(CGRect)frame centerPosition:(CGPoint)position imageURL:(NSString *)imageUrl;
- (void)addMoveAnitationToPoint:(CGPoint)point;
@end
