//
//  ParallaxView.m
//  InstagramClient
//
//  Created by Artem on 22/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "ParallaxView.h"
#import "LazyLoadingImageView.h"

#define kxPadding 15.f
#define kyPadding 30.f

#define kMotionLeftRight 25.f
#define kMotionUpDown 15.f

@interface ParallaxView(){
    LazyLoadingImageView *_imageView;
}

@end

@implementation ParallaxView
- (instancetype)initWithFrame:(CGRect)frame centerPosition:(CGPoint)position imageURL:(NSString *)imageUrl {
    if (self = [super initWithFrame:frame]) {
        self.center = position;
        _imageView = [[LazyLoadingImageView alloc] initWithFrame:CGRectMake(  - kxPadding,
                                                                              - kyPadding,
                                                                            CGRectGetWidth(frame) + kxPadding * 2.f,
                                                                            CGRectGetHeight(frame) + kyPadding * 2.f)];
        [_imageView loadImageFromURL:imageUrl];
        
        [self addSubview:_imageView];
        [self initMontionEffect];
    }
    return self;
}

- (void)addMoveAnitationToPoint:(CGPoint)point {

    self.layer.drawsAsynchronously = YES;

    CGPoint startPoint = self.center;
    CGPoint endPoint = point;

    CGMutablePathRef thePath = CGPathCreateMutable();
    CGPathMoveToPoint(thePath, NULL, startPoint.x, startPoint.y);
    CGPathAddLineToPoint(thePath, NULL, endPoint.x, endPoint.y);

    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.duration = 10.f;
    animation.path = thePath;
    animation.autoreverses = YES;
    animation.repeatCount = INFINITY;
    [self.layer addAnimation:animation forKey:@"position"];
}

#pragma mark -
- (void)initMontionEffect {

    UIInterpolatingMotionEffect *leftRight = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];

    leftRight.minimumRelativeValue = @(-kMotionLeftRight);
    leftRight.maximumRelativeValue = @(kMotionLeftRight);

    UIInterpolatingMotionEffect *upDown = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];

    upDown.minimumRelativeValue = @(-kMotionUpDown);
    upDown.maximumRelativeValue = @(kMotionUpDown);

    UIMotionEffectGroup *motionGroup = [UIMotionEffectGroup new];
    motionGroup.motionEffects = @[leftRight,upDown];

    [_imageView addMotionEffect:motionGroup];

}
@end
