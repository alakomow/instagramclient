//
//  AuthViewController.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TypeAuthCompletionBlock)(NSString *accessToken, NSError *error);

@interface AuthViewController : UIViewController

+ (id) init __attribute__((unavailable("Must use other methods.")));
+ (id) new __attribute__((unavailable("Must use other methods.")));

- (id) init __attribute__((unavailable("Must use other methods.")));
- (id) initWithCoder:(NSCoder *)aDecoder __attribute__((unavailable("Must use other methods.")));

- (instancetype)initWithCompletion:(TypeAuthCompletionBlock)completion;
@end
