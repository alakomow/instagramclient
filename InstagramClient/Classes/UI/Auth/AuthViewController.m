//
//  AuthViewController.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "AuthViewController.h"
#import "APIOperation.h"
#import "NSURL+Parameters.h"

#define kAccessTokenParamKey @"access_token"

@interface AuthViewController () <UIWebViewDelegate>{
    TypeAuthCompletionBlock _completion;
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation AuthViewController

- (instancetype)initWithCompletion:(TypeAuthCompletionBlock)completion {
    if (self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil]) {
        NSAssert(completion, @"Completion must be not NULL");
        _completion = [completion copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[APIOperation authorizationURL]]];

    [self.navigationItem setTitle:NSLocalizedString(@"AuthorizationControllerTitle", nil)];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.absoluteString hasPrefix:kAPIRedirectURL]) {
        NSString *accessKey = [request.URL stringValueForParamKey:kAccessTokenParamKey];
        NSError *error;
        if (!accessKey.length) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:NSLocalizedString(@"AuthorizationErrorDescription", nil) forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:APIErrorDomain code:APIErrorCodeAuthorizationError userInfo:details];
        }
        _completion(accessKey, error);
        return NO;
    }
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if (error.code == NSURLErrorCancelled) return;
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    
    _completion(nil, error);
}
@end
