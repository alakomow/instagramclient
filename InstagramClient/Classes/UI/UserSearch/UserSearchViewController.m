//
//  UserSearchViewController.m
//  InstagramClient
//
//  Created by Artem on 24/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "UserSearchViewController.h"
#import "UserSearchViewCell.h"

#import "UIAlertView+ErrorMessage.h"

#import "NSManagedObjectContext+DataBaseHelper.h"
#import "NSManagedObject+Wrapper.h"
#import "User.h"

#import "NSUserDefaults+AccessToken.h"
#import "APIOperation.h"
#import "MappingOperation.h"

#define kSearchBarPlaceholderKey @"SearchUserBarPlaceHolder"
#define kTimeAfterSendingSearchRequest 2.f



@interface UserSearchViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NSFetchedResultsControllerDelegate> {
    NSTimer *_searchTimer;
    NSFetchedResultsController *_contentController;
    UIView *_loadingIndicatorView;
    APIOperation *_searchOperation;
}

@end

@implementation UserSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"TMP";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.placeholder = NSLocalizedString(kSearchBarPlaceholderKey, nil);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.searchBar becomeFirstResponder];
    UIEdgeInsets tableViewContentInset = self.tableView.contentInset;
    tableViewContentInset.top = CGRectGetMaxY(self.navigationController.navigationBar.frame);
    self.tableView.contentInset = tableViewContentInset;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loadingIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                     0.f,
                                                                     CGRectGetWidth(self.tableView.frame),
                                                                     CGRectGetHeight(loadingIndicator.frame) * 2.f)];
    [_loadingIndicatorView addSubview:loadingIndicator];
    loadingIndicator.center = CGPointMake(CGRectGetWidth(_loadingIndicatorView.frame)/2.f,
                                          CGRectGetHeight(_loadingIndicatorView.frame)/2.f);
    [loadingIndicator startAnimating];
    [_loadingIndicatorView setBackgroundColor:[UIColor lightGrayColor]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_searchTimer invalidate];
    _searchTimer = nil;

    [_searchOperation cancel];
    _searchOperation = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [_searchOperation cancel];
    _searchOperation = nil;
    [_searchTimer invalidate];
    _searchTimer = nil;
    self.tableView.tableHeaderView = nil;
}
#pragma mark -
- (void)makeSearchUserRequest {
    NSString *queryString = self.searchBar.text;
    if (!queryString){
        self.tableView.tableHeaderView = nil;
        return;
    }

    [_searchTimer invalidate];
    _searchTimer = nil;

    [_searchOperation cancel];

    __weak typeof(self) weakSelf = self;
    _searchOperation = [[APIOperation alloc] initSearchUserByUsername:queryString accessToken:[NSUserDefaults accessToken] completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (error) {
            if (didCanceled) {
                [weakSelf searchDidSuccess];
                return;
            }
            [weakSelf searchDidError:error];
            return;
        }
        [[[MappingOperation alloc] initWithData:[responce objectForKey:@"data"] entityClass:[User class] completionBlock:^(NSManagedObjectContext *context, NSArray *objects, NSError *error) {
            if (error) {
                [weakSelf searchDidError:error];
                return;
            }
            [weakSelf searchDidSuccess];
        }] start];
    }];
    [_searchOperation start];
}

- (void)searchDidSuccess {
    self.tableView.tableHeaderView = nil;
    _searchOperation = nil;
}

- (void)searchDidError:(NSError *)error {
    [[[UIAlertView alloc] initWithError:error completion:^{
        [self popViewController];
    }] show];
    self.tableView.tableHeaderView = nil;
    _searchOperation = nil;
}

- (NSFetchedResultsController *)contentController {
    if (_contentController)
        return _contentController;

    NSString *searchQuery = self.searchBar.text;
    NSFetchRequest *request = [User entityFetchRequestInContext:[NSManagedObjectContext defaultContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userName CONTAINS[c] %@) OR (fullName CONTAINS[c] %@)",searchQuery,searchQuery];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userName" ascending:NO];

    request.predicate = predicate;
    request.sortDescriptors = @[sortDescriptor];

    NSString *cacheName = NSStringFromClass([self class]);
    [NSFetchedResultsController deleteCacheWithName:cacheName];

    _contentController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[NSManagedObjectContext defaultContext] sectionNameKeyPath:nil cacheName:cacheName];
    _contentController.delegate = self;

    NSError *error;
    if (![_contentController performFetch:&error]) {
        [[[UIAlertView alloc] initWithError:error completion:NULL] show];
    }
    return _contentController;
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Actions
- (IBAction)canceSearch:(id)sender {
    [self popViewController];
}

#pragma mark - Table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[[self contentController] sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[[self contentController] sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"SearchUserCell";
    UserSearchViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    User *user = [[self contentController] objectAtIndexPath:indexPath];
    cell.userNameLabel.text = user.userName;
    cell.userFullNameLabel.text = user.fullName;
    [cell.userPictureView loadImageFromURL:user.profilePicture];
    cell.userPictureView.layer.cornerRadius = CGRectGetHeight(cell.userPictureView.frame) / 2.f;
    cell.userPictureView.clipsToBounds = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = [[self contentController] objectAtIndexPath:indexPath];
    if ([self.delegate respondsToSelector:@selector(userSearchController:didFoundUser:)]) {
        [self.delegate userSearchController:self didFoundUser:user];
    }
    [self popViewController];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark - UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_searchTimer invalidate];
    _searchTimer = nil;
    _searchTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeAfterSendingSearchRequest target:self selector:@selector(makeSearchUserRequest) userInfo:nil repeats:NO];

    _contentController = nil;
    self.tableView.tableHeaderView = _loadingIndicatorView;
    [self.tableView reloadData];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
