//
//  UserSearchViewController.h
//  InstagramClient
//
//  Created by Artem on 24/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserSearchViewControllerDelegate;
@class User;

@interface UserSearchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) id<UserSearchViewControllerDelegate> delegate;

@end

@protocol UserSearchViewControllerDelegate <NSObject>
@required
- (void)userSearchController:(UserSearchViewController *)controller didFoundUser:(User *)user;
@end
