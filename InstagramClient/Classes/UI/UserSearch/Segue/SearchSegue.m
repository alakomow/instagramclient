//
//  SearchSegue.m
//  InstagramClient
//
//  Created by Artem on 24/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "SearchSegue.h"

@implementation SearchSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    UINavigationController *navigationController = sourceViewController.navigationController;

    UIImage *bgImage = [self backgroundImage];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:bgImage];
    imageView.frame = (CGRect){CGPointZero, bgImage.size};

    UIView *view = [[UIView alloc] initWithFrame:imageView.frame];
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];

    [destinationViewController.view setUserInteractionEnabled:NO];

    [destinationViewController.view insertSubview:view atIndex:0];
    [destinationViewController.view insertSubview:imageView atIndex:0];

    [CATransaction begin];

    CATransition *transition = [CATransition animation];
    transition.type = @"fade";
    transition.duration = 0.4f;
    transition.fillMode = kCAFillModeBackwards;
    transition.removedOnCompletion = YES;
    transition.delegate = self;


    [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:nil];

    //[navigationController presentViewController:destinationViewController animated:YES completion:NULL];
    [navigationController pushViewController:destinationViewController animated:NO];
   // [navigationController showViewController:destinationViewController sender:nil];

    [CATransaction commit];

}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    UIViewController *controller = self.destinationViewController;
    if  (controller) [controller.view setUserInteractionEnabled:YES];
}

#pragma mark -
-(UIImage*)backgroundImage
{

    UIViewController *sourceViewController = self.sourceViewController;
    UINavigationController *navigationController = sourceViewController.navigationController;

    CGSize size = navigationController.view.bounds.size;
    UIGraphicsBeginImageContextWithOptions(size, YES, [[UIScreen mainScreen] scale]);
    [navigationController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}
@end
