//
//  NSUserDefaults+AccessToken.m
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "NSUserDefaults+AccessToken.h"

#define kAccessTokenKey @"kAccessTokenKey"

@implementation NSUserDefaults (AccessToken)
+ (NSString *)accessToken {
    NSString *token = [[self standardUserDefaults] stringForKey:kAccessTokenKey];
    return token.length?token:nil;
}

+ (void)setAccessToken:(NSString *)accessToken {
    NSUserDefaults *settings = [self standardUserDefaults];
    [settings setObject:accessToken forKey:kAccessTokenKey];
    [settings synchronize];
}
@end
