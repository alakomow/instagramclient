//
//  NSUserDefaults+AccessToken.h
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (AccessToken)
+ (NSString *)accessToken;
+ (void)setAccessToken:(NSString *)accessToken;
@end
