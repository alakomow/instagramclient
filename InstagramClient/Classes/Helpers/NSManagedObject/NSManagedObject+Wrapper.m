//
//  NSManagedObject+Wrapper.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "NSManagedObject+Wrapper.h"

@implementation NSManagedObject (Wrapper)
+ (instancetype)newObjectInContext:(NSManagedObjectContext *)context {
    NSEntityDescription *entityDescription = [self entityDescriptionInContext:context];
    id object = [[self alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    return object;
}

+ (NSUInteger)countOfObjectsInContext:(NSManagedObjectContext *)context {

    NSUInteger cnt = [context countForFetchRequest:[self entityFetchRequestInContext:context] error:nil];
    return cnt;
}
+ (instancetype)findFirstByAtribute:(NSString *)atribute value:(id)value inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [self entityFetchRequestInContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",atribute,value];
    request.predicate = predicate;
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    return objects.firstObject;
}

+ (NSFetchRequest *)entityFetchRequestInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:[self entityDescriptionInContext:context]];
    return request;
}


+ (NSEntityDescription *)entityDescriptionInContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription entityForName:[self entityName] inManagedObjectContext:context];;
}

+ (NSString *)entityName {
    NSString *entityName;
    if ([entityName length] == 0)
    {
        entityName = [NSStringFromClass(self) componentsSeparatedByString:@"."].lastObject;
    }

    return entityName;
}
@end
