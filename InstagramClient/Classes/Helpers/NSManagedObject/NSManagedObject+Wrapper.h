//
//  NSManagedObject+Wrapper.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Wrapper)
+ (instancetype)newObjectInContext:(NSManagedObjectContext *)context;
+ (NSUInteger)countOfObjectsInContext:(NSManagedObjectContext *)context;
+ (instancetype)findFirstByAtribute:(NSString *)atribute value:(id)value inContext:(NSManagedObjectContext *)context;
+ (NSFetchRequest *)entityFetchRequestInContext:(NSManagedObjectContext *)context;
@end
