//
//  NSURL+Parameters.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Parameters)
- (NSString *)stringValueForParamKey:(NSString *)key;
@end
