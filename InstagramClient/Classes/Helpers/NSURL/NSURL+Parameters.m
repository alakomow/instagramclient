//
//  NSURL+Parameters.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "NSURL+Parameters.h"

@implementation NSURL (Parameters)
- (NSString *)stringValueForParamKey:(NSString *)key {
    NSArray *params = [self.fragment componentsSeparatedByString:@"&"];
    for (NSString *param in params) {
        NSArray *keyValue = [param componentsSeparatedByString:@"="];
        if (keyValue.count == 2 &&
            [keyValue.firstObject isEqualToString:key]) {
            return keyValue.lastObject;
        }
    }
    return nil;
}
@end
