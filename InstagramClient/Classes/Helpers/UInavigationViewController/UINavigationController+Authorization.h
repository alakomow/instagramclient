//
//  UINavigationController+Authorization.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Authorization)
- (void)checkAPIAuthorization;
@end
