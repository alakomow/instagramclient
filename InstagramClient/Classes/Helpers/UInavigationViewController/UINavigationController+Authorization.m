//
//  UINavigationController+Authorization.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "UINavigationController+Authorization.h"
#import "NSUserDefaults+AccessToken.h"
#import "AuthViewController.h"
#import "UIAlertView+ErrorMessage.h"

@implementation UINavigationController (Authorization)
- (void)checkAPIAuthorization {
    NSString *accessToken = [NSUserDefaults accessToken];
    if (!accessToken) {

        __weak typeof(self) weakSelf = self;
        AuthViewController *authController = [[AuthViewController alloc] initWithCompletion:^(NSString *accessToken, NSError *error) {
            if (error) {
                [[[UIAlertView alloc] initWithError:error completion:^{
                    [weakSelf popViewControllerAnimated:YES];
                }] show];
            } else {
                [NSUserDefaults setAccessToken:accessToken];
                [weakSelf popViewControllerAnimated:YES];
            }

        }];
        [self pushViewController:authController animated:YES];
    }
}

@end
