//
//  UIAlertView+ErrorMessage.h
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UIAlertViewErrorCompletionBlock)();

@interface UIAlertView (ErrorMessage)
- (instancetype)initWithError:(NSError *)error completion:(UIAlertViewErrorCompletionBlock)completion;
@end
