//
//  UIAlertView+ErrorMessage.m
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <objc/runtime.h>

#import "UIAlertView+ErrorMessage.h"

#define kCompletionKey  @"kCompletionKey"

@interface UIAlertView() <UIAlertViewDelegate>
@end

@implementation UIAlertView (ErrorMessage)
- (instancetype)initWithError:(NSError *)error completion:(UIAlertViewErrorCompletionBlock)completion {

    if (self = [self initWithTitle:NSLocalizedString(@"ErrorTitle", nil)
                           message:[error localizedDescription]
                          delegate:self
                 cancelButtonTitle:NSLocalizedString(@"OkButtonTitle", nil)
                 otherButtonTitles:nil]) {
        self.completion = completion;
    }
    return self;
}

#pragma mark - completion
- (void)setCompletion:(UIAlertViewErrorCompletionBlock)completion
{
    objc_setAssociatedObject(self, kCompletionKey, completion, OBJC_ASSOCIATION_COPY);
}

- (UIAlertViewErrorCompletionBlock)completion
{
    return objc_getAssociatedObject(self, kCompletionKey);
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.completion) {
        self.completion();
    }
}
@end
