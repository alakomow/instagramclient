//
//  NSManagedObjectContext+DataBaseHelper.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIApplication.h>

#import "NSManagedObjectContext+DataBaseHelper.h"

@implementation NSManagedObjectContext (DataBaseHelper)
+ (instancetype)defaultContext {
    id delegate = [[UIApplication sharedApplication] delegate];
    id context = [delegate managedObjectContext];
    return context;
}

+ (instancetype)newContext {
    NSManagedObjectContext *context = [[self alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.parentContext = [self defaultContext];
    return context;
}

- (void)saveWithCompletion:(SaveContextCompletionHandler)completion {
    __block BOOL hasChanges = NO;

    if ([self concurrencyType] == NSConfinementConcurrencyType)
    {
        hasChanges = [self hasChanges];
    }
    else
    {
        [self performBlockAndWait:^{
            hasChanges = [self hasChanges];
        }];
    }

    if (!hasChanges)
    {
        if (completion)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(NO, nil);
            });
        }

        return;
    }

    [self performBlock:^{
        NSError *error = nil;
        if (hasChanges && ![self save:&error]) {
            if (completion) {
                completion(NO,error);
            }
            return;
        }

        if (self.parentContext) {
            [self.parentContext saveWithCompletion:completion];
        } else {
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil);
                });
            }
        }
    }];
}
@end
