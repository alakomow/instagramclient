//
//  NSManagedObjectContext+DataBaseHelper.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <CoreData/CoreData.h>

typedef void (^SaveContextCompletionHandler)(BOOL contextDidSave, NSError *error);

@interface NSManagedObjectContext (DataBaseHelper)
+ (instancetype)defaultContext;
+ (instancetype)newContext;

- (void)saveWithCompletion:(SaveContextCompletionHandler)completion;
@end
