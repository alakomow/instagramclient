//
//  User.m
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "User.h"
#import "Comment.h"
#import "Media.h"
#import "User.h"


@implementation User

@dynamic fullName;
@dynamic profilePicture;
@dynamic userID;
@dynamic userName;
@dynamic follows;
@dynamic folowedBy;
@dynamic mediaFiles;
@dynamic comments;

@end
