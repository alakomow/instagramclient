//
//  Comment.m
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Comment.h"
#import "Media.h"
#import "User.h"


@implementation Comment

@dynamic commentID;
@dynamic createdTime;
@dynamic text;
@dynamic user;
@dynamic media;

@end
