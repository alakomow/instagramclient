//
//  User.h
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment, Media, User;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * profilePicture;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *follows;
@property (nonatomic, retain) NSSet *folowedBy;
@property (nonatomic, retain) NSSet *mediaFiles;
@property (nonatomic, retain) NSSet *comments;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addFollowsObject:(User *)value;
- (void)removeFollowsObject:(User *)value;
- (void)addFollows:(NSSet *)values;
- (void)removeFollows:(NSSet *)values;

- (void)addFolowedByObject:(User *)value;
- (void)removeFolowedByObject:(User *)value;
- (void)addFolowedBy:(NSSet *)values;
- (void)removeFolowedBy:(NSSet *)values;

- (void)addMediaFilesObject:(Media *)value;
- (void)removeMediaFilesObject:(Media *)value;
- (void)addMediaFiles:(NSSet *)values;
- (void)removeMediaFiles:(NSSet *)values;

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

@end
