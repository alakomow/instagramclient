//
//  Media.h
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment, User;

@interface Media : NSManagedObject

@property (nonatomic, retain) NSDate * createdTime;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSNumber * likeCount;
@property (nonatomic, retain) NSString * mediaID;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSSet *comments;
@end

@interface Media (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

@end
