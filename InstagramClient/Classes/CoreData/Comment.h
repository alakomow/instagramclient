//
//  Comment.h
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Media, User;

@interface Comment : NSManagedObject

@property (nonatomic, retain) NSString * commentID;
@property (nonatomic, retain) NSDate * createdTime;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) Media *media;

@end
