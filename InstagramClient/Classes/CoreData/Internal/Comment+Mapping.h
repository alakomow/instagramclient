//
//  Comment+Mapping.h
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Comment.h"
#import "MappingProtocol.h"
@interface Comment (Mapping) <MappingProtocol>

@end
