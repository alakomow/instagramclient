//
//  User+Mapping.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "User+Mapping.h"
#import "NSManagedObject+Wrapper.h"

@implementation User (Mapping)

+ (NSArray *)objectsFromArray:(NSArray *)data inContext:(NSManagedObjectContext *)context {
    NSMutableArray *objects = [NSMutableArray new];
    for (id dictionary in data) {
        if (![dictionary isKindOfClass:[NSDictionary class]]) continue;
        id object = [self objectFromDictionary:dictionary inContext:context];
        if (object) [objects addObject:object];
    }
    return [NSArray arrayWithArray:objects];
}

+ (instancetype)objectFromDictionary:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    id userID = [data objectForKey:@"id"];
    NSAssert(userID, @"Primary key Id not found.");
    if (!userID)
        return nil;

   User *object = [self findFirstByAtribute:NSStringFromSelector(@selector(userID)) value:@([userID integerValue]) inContext:context];
    if (!object) {
        object = [self newObjectInContext:context];
    }

    object.userID = @([[data objectForKey:@"id"] longLongValue]);
    object.userName = [data objectForKey:@"username"];
    object.fullName = [data objectForKey:@"full_name"];
    object.profilePicture = [data objectForKey:@"profile_picture"];
    return object;
}
@end
