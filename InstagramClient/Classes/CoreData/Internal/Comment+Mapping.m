//
//  Comment+Mapping.m
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Comment+Mapping.h"
#import "NSManagedObject+Wrapper.h"
#import "User+Mapping.h"

@implementation Comment (Mapping)

+ (NSArray *)objectsFromArray:(NSArray *)data inContext:(NSManagedObjectContext *)context {
    NSMutableArray *objects = [NSMutableArray new];
    for (id dictionary in data) {
        if (![dictionary isKindOfClass:[NSDictionary class]]) continue;
        id object = [self objectFromDictionary:dictionary inContext:context];
        if (object) [objects addObject:object];
    }
    return [NSArray arrayWithArray:objects];
}

+ (instancetype)objectFromDictionary:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    id commentID = [data objectForKey:@"id"];
    NSAssert(commentID, @"Primary key Id not found.");
    if (!commentID)
        return nil;

    Comment *object = [self findFirstByAtribute:NSStringFromSelector(@selector(commentID)) value:commentID inContext:context];
    if (!object) {
        object = [self newObjectInContext:context];
    }

    object.commentID = [data objectForKey:@"id"];
    object.createdTime = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"created_time"] doubleValue]];
    object.text = [data objectForKey:@"text"];
    object.user = [User objectFromDictionary:[data objectForKey:@"from"] inContext:context];
   
    return object;
}
@end
