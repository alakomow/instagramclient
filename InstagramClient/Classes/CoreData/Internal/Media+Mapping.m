//
//  Media+Mapping.m
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Media+Mapping.h"
#import "NSManagedObject+Wrapper.h"
#import "User+Mapping.h"

@implementation Media (Mapping)
+ (NSArray *)objectsFromArray:(NSArray *)data inContext:(NSManagedObjectContext *)context {
    NSMutableArray *objects = [NSMutableArray new];
    for (id dictionary in data) {
        if (![dictionary isKindOfClass:[NSDictionary class]]) continue;
        id object = [self objectFromDictionary:dictionary inContext:context];
        if (object) [objects addObject:object];
    }
    return [NSArray arrayWithArray:objects];
}

+ (instancetype)objectFromDictionary:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    id mediaID = [data objectForKey:@"id"];
    NSAssert(mediaID, @"Primary key Id not found.");
    if (!mediaID)
        return nil;

    Media *object = [self findFirstByAtribute:NSStringFromSelector(@selector(mediaID)) value:mediaID inContext:context];
    if (!object) {
        object = [self newObjectInContext:context];
    }

    object.mediaID = [data objectForKey:@"id"];
    object.createdTime = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"created_time"] doubleValue]];
    object.type = [data objectForKey:@"type"];

    id images = [data objectForKey:@"images"];
    NSString *imageURL;
    if ([images isKindOfClass:[NSDictionary class]]) {
        NSUInteger maxSize = 0;
        for (NSDictionary *image in [images allValues]) {
            NSUInteger curSize = MAX([[image objectForKey:@"height"] integerValue],[[image objectForKey:@"width"] integerValue]);
            if (curSize > maxSize) {
                maxSize = curSize;
                imageURL = [image objectForKey:@"url"];
            }
        }
    }
    object.imageURL = imageURL;

    User *user = [User objectFromDictionary:[data objectForKey:@"user"] inContext:context];
    object.user = user;

    id likes = [data objectForKey:@"likes"];
    if ([likes isKindOfClass:[NSDictionary class]]) {
        object.likeCount = @([[likes objectForKey:@"count"] integerValue]);
    }

    return object;
}
@end
