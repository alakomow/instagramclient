//
//  Media+Mapping.h
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Media.h"
#import "MappingProtocol.h"
@interface Media (Mapping) <MappingProtocol>

@end
