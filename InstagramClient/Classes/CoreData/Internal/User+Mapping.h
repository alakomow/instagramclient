//
//  User+Mapping.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "User.h"
#import "MappingProtocol.h"

@interface User (Mapping) <MappingProtocol>
@end
