//
//  MappingProtocol.h
//  InstagramClient
//
//  Created by Artem on 21/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//
#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@protocol MappingProtocol <NSObject>
+ (NSArray *)objectsFromArray:(NSArray *)data inContext:(NSManagedObjectContext *)context;
+ (instancetype)objectFromDictionary:(NSDictionary *)data inContext:(NSManagedObjectContext *)context;
@end

