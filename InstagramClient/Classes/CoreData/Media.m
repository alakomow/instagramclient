//
//  Media.m
//  InstagramClient
//
//  Created by Artem on 23/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "Media.h"
#import "Comment.h"
#import "User.h"


@implementation Media

@dynamic createdTime;
@dynamic imageURL;
@dynamic likeCount;
@dynamic mediaID;
@dynamic type;
@dynamic user;
@dynamic comments;

@end
