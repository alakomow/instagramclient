//
//  NSmanagedObjectsInternal.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#ifndef InstagramClient_NSmanagedObjectsInternal_h
#define InstagramClient_NSmanagedObjectsInternal_h
#import "MappingProtocol.h"
#import "User+Mapping.h"
#import "Media+Mapping.h"
#import "Comment+Mapping.h"
#endif
