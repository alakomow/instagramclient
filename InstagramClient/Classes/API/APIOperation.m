//
//  APIOperation.m
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "APIOperation.h"
#import "UIAlertView+ErrorMessage.h"

NSString *const kAPIRedirectURL = @"http://localhost";
NSString *const APIErrorDomain = @"InstagramAPIError";

#define kAPIClientID @"c0496eccd96b4fc2986068e86d3bf18a"
#define kAPIAuthorizationURL @"https://instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token"
#define kAPIUrl @"https://api.instagram.com/"

#define kRequestTimeOutInterval 60.f

@interface APIOperation(){
    APIOperationCompletionBlock _completion;
    NSURLRequest *_request;
    NSURLSessionDataTask *_dataTask;
}

@end

@implementation APIOperation

+ (NSURL *)authorizationURL {
    NSString *urlString = [NSString stringWithFormat:kAPIAuthorizationURL,kAPIClientID,kAPIRedirectURL];
    return [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (instancetype)initSearchUserByUsername:(NSString *)username
                             accessToken:(NSString *)accessToken
                              completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _request = [NSURLRequest requestWithURL:[self searchUserURLForUsername:username accessToken:accessToken]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initMediaRecentForIsedID:(NSNumber *)userID
                             accessToken:(NSString *)accessToken
                              maxDate:(NSDate *)maxDate
                              completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        NSURL *url = [self mediaRecentURLWithUserID:userID maxDate:maxDate accessToken:accessToken];
        _request = [NSURLRequest requestWithURL:url
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initFollowsForUserID:(NSNumber *)userID accessToken:(NSString *)accessToken completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _request = [NSURLRequest requestWithURL:[self followsURLForUserID:userID accessToken:accessToken]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initSearchLocationWithAccessToken:(NSString *)accessToken lat:(NSNumber *)lat lon:(NSNumber *)lon distance:(NSNumber *)distance completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _request = [NSURLRequest requestWithURL:[self locationSearchURLWithAccesstoken:accessToken
                                                                                   lat:lat
                                                                                   lon:lon
                                                                              distance:distance]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initSearchMediaRecentWithLocationID:(NSString *)locatioID accessToken:(NSString *)accessToken completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _request = [NSURLRequest requestWithURL:[self mediaRecentWithLocationID:locatioID
                                                                    accessToken:accessToken]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initCommentWithMediaID:(NSString *)mediaID accessToken:(NSString *)accessToken completion:(APIOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _request = [NSURLRequest requestWithURL:[self urlForCommentsByMediaID:mediaID accessToken:accessToken] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:kRequestTimeOutInterval];
    }
    return self;
}

- (instancetype)initWithCompletion:(APIOperationCompletionBlock)complition {
    if (self = [super init]) {
        _completion = [complition copy];
    }
    return self;
}

#pragma mark -

- (void)start {
    _isRunning = YES;


    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
     _dataTask = [session dataTaskWithRequest:_request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         BOOL isCanceled = NO;
        if (error) {
            if ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorCancelled) {
                isCanceled = YES;
            }
            if (_completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _isRunning = NO;
                    _completion(nil,isCanceled,error);
                });
            }
            return;
        }

        NSDictionary *responceData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
         NSString *errorMsg = [responceData objectForKey:@"error_message"];
         if (errorMsg.length) {
             NSMutableDictionary* details = [NSMutableDictionary dictionary];
             [details setValue:errorMsg forKey:NSLocalizedDescriptionKey];
             error = [NSError errorWithDomain:APIErrorDomain code:[[responceData objectForKey:@"code"] integerValue] userInfo:details];
         }
        dispatch_async(dispatch_get_main_queue(), ^{
            _isRunning = NO;
            _completion(responceData, isCanceled, error);
        });
    }];

    [_dataTask resume];
}

- (void)cancel {
    [_dataTask cancel];
}

#pragma mark - URLs
- (NSURL *)searchUserURLForUsername:(NSString *)username accessToken:(NSString *)token {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/users/search?q=%@&access_token=%@",username,token];
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)mediaRecentURLWithUserID:(NSNumber *)userID maxDate:(NSDate *)maxDate accessToken:(NSString *)accessToken {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/users/%@/media/recent/?access_token=%@",userID, accessToken];
    if (maxDate) {
        stringURL = [stringURL stringByAppendingFormat:@"&max_timestamp=%@",@(maxDate.timeIntervalSince1970)];
    }
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)followsURLForUserID:(NSNumber *)userID accessToken:(NSString *)accessToken {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/users/%@/follows?access_token=%@",userID,accessToken];
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)locationSearchURLWithAccesstoken:(NSString *)accessToke lat:(NSNumber *)lat lon:(NSNumber *)lon distance:(NSNumber *)distance {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/locations/search?lat=%@&lng=%@&access_token=%@&distance=%@",lat,lon,accessToke,distance];
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)mediaRecentWithLocationID:(NSString *)locationID accessToken:(NSString *)accessToken {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/locations/%@/media/recent?access_token=%@",locationID,accessToken];
    return [NSURL URLWithString:stringURL];
}

- (NSURL *)urlForCommentsByMediaID:(NSString *)mediaID accessToken:(NSString *)accessToken {
    NSString *stringURL = [kAPIUrl stringByAppendingFormat:@"v1/media/%@/comments?access_token=%@",mediaID,accessToken];
    return [NSURL URLWithString:stringURL];
}

@end

