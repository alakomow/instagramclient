//
//  APIOperation.h
//  InstagramClient
//
//  Created by Artem on 19/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kAPIRedirectURL;
extern NSString *const APIErrorDomain;

typedef NS_ENUM(NSUInteger, APIErrorCode) {
    APIErrorCodeAuthorizationError = 1,
};

typedef void(^APIOperationCompletionBlock)(NSDictionary *responce, BOOL didCanceled, NSError *error);

@interface APIOperation : NSObject
- (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) new __attribute__((unavailable("Must use other init methods.")));

@property (nonatomic, readonly)BOOL isRunning;


- (instancetype)initSearchUserByUsername:(NSString *)username
                             accessToken:(NSString *)accessToken
                              completion:(APIOperationCompletionBlock)completion;

- (instancetype)initMediaRecentForIsedID:(NSNumber *)userID
                             accessToken:(NSString *)accessToken
                              maxDate:(NSDate *)maxDate
                              completion:(APIOperationCompletionBlock)completion;

- (instancetype)initFollowsForUserID:(NSNumber *)userID
                         accessToken:(NSString *)accessToken
                           completion:(APIOperationCompletionBlock)completion;

- (instancetype)initSearchLocationWithAccessToken:(NSString *)accessToken
                                              lat:(NSNumber *)lat
                                              lon:(NSNumber *)lon
                                         distance:(NSNumber *)distance
                                       completion:(APIOperationCompletionBlock)completion;

- (instancetype)initSearchMediaRecentWithLocationID:(NSString *)locatioID
                                        accessToken:(NSString *)accessToken
                                         completion:(APIOperationCompletionBlock)completion;

- (instancetype)initCommentWithMediaID:(NSString *)mediaID
                           accessToken:(NSString *)accessToken
                            completion:(APIOperationCompletionBlock)completion;

- (void)start;
- (void)cancel;
@end

#pragma mark - Authorization

@interface APIOperation(Authorization)
+ (NSURL *)authorizationURL;
@end
