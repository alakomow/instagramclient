//
//  MappingOperation.h
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;

typedef void(^MappingOperationCompletionBlock)(NSManagedObjectContext *context,NSArray *objects, NSError *error);

@interface MappingOperation : NSObject


- (instancetype)initWithData:(NSArray *)data entityClass:(Class)entityClass completionBlock:(MappingOperationCompletionBlock)completion;
- (void)start;
@end

