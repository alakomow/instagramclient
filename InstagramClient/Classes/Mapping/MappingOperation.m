//
//  MappingOperation.m
//  InstagramClient
//
//  Created by Artem on 20/06/15.
//  Copyright (c) 2015 Artem Lakomow. All rights reserved.
//

#import "MappingOperation.h"
#import "NSManagedObjectContext+DataBaseHelper.h"
#import "NSManagedObject+Wrapper.h"
#import "NSmanagedObjectsInternal.h"

NSString *const MappingPrimaryKey = @"MappingPrimaryKey";

@interface MappingOperation(){
    NSArray *_data;
    Class _entityClass;
    MappingOperationCompletionBlock _completionBlock;
}

@end

@implementation MappingOperation
- (instancetype)initWithData:(NSArray *)data entityClass:(Class)entityClass completionBlock:(MappingOperationCompletionBlock)completion {

    if (self = [super init]) {
        NSAssert([entityClass isSubclassOfClass:[NSManagedObject class]], @"Class must be NSmanagedObject subclass");
        NSAssert([entityClass conformsToProtocol:@protocol(MappingProtocol)], @"Class must be confirmed Mapping Protocol");
        _data = data;
        _entityClass = entityClass;
        _completionBlock = [completion copy];
    }

    return self;
}

- (void)start {
    [self startAsync];
}

- (void)startAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *context = [NSManagedObjectContext newContext];
         NSArray *objects = [_entityClass objectsFromArray:_data inContext:context];
        [context saveWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (error) {
                if (_completionBlock) {
                    _completionBlock(context, objects, error);
                }
                return;
            }

            if (_completionBlock) {
                _completionBlock(context, objects, nil);
            }
        }];
    });
}
@end
